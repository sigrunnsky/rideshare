class CreateRides < ActiveRecord::Migration
  def change
    create_table :rides do |t|
      t.string :to_location
      t.string :from_location
      t.datetime :leaving_at
      t.integer :driver_user_id
      t.decimal :price_per_passenger
      t.integer :car_capacity

      t.timestamps null: false
    end
  end
end
