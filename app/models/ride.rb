class Ride < ActiveRecord::Base
  belongs_to :driver_user, :class_name => 'User'
  default_scope -> { order(created_at: :desc) }
end
