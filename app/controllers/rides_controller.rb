class RidesController < ApplicationController
  def create
    @ride = current_user.rides.build(ride_params)
    @ride.driver_user_id = current_user.id
    if @ride.save
      flash[:success] = "Ride created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end
  private

    def ride_params
      params.require(:ride).permit(:to_location, :from_location, :leaving_at, :price_per_passenger, :car_capacity)
    end
  
end