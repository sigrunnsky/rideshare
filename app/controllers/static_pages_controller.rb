class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @ride  = current_user.rides.build
      @rides = Ride.paginate(page: params[:page] || 1)
      @feed_items = Micropost.paginate(page: params[:page] || 1)
    end
  end
  
  def about
  end

  def help
  end
  
  def contact
  end
end
